#!/bin/sh
set -e
export HOME=`pwd`

TARGETS=${TARGETS:-"linux/amd64 linux/arm linux/arm64 windows/amd64 windows/arm64"}
BUILD_LDFLAGS=${BUILD_LDFLAGS:-"-s -w -buildid="}
BINARY_DIR=$HOME/binary
MARCHAPP_DIR=$HOME/marchbin
VERSION=${VERSION:-"main"}
export CGO_ENABLED=${CGO_ENABLED:-'0'}
AMD64_MARCHES="v1 v2 v3 v4"
ARM_MARCHES="7"

function goBuild() {
  #GOOS=windows GOARCH=amd64 GOAMD64=v2 goBuild output_fimename source_path build_tags
  BUILD_SRCFILENAME=${2:-'.'}
  BUILD_BINFILEEXTNAME=''
  GOARCH=${GOARCH:-'amd64'}
  GOOS=${GOOS:-'linux'}
  BUILD_CPUARCHNAME=$GOARCH
  if [ "x$3" = "x" ]; then
    BUILD_TAGOPT=""
  else
    BUILD_TAGOPT="-tags $3"
  fi
  if [ "$GOARCH" = "amd64" ]; then
    GOAMD64=${GOAMD64:-'v1'}
    BUILD_CPUARCHNAME='x86_64-'$GOAMD64
  fi
  if [ "$GOARCH" = "arm" ]; then
    GOARM=${GOARM:-'6'}
    BUILD_CPUARCHNAME='armv'$GOARM
  fi
  if [ "$GOARCH" = "386" ]; then
    BUILD_CPUARCHNAME='x86'
  fi
  if [ "$GOOS" = "windows" ]; then
    BUILD_BINFILEEXTNAME='.exe'
  fi
  echo "build ${1##*/} for $GOOS on $BUILD_CPUARCHNAME"
  go build -trimpath -ldflags="$BUILD_LDFLAGS" -o $1-$GOOS-$BUILD_CPUARCHNAME$BUILD_BINFILEEXTNAME $BUILD_TAGOPT $BUILD_SRCFILENAME
}

echo "Building marchbin"
mkdir $MARCHAPP_DIR
cd gosrc
mkdir temp
GOARCH=amd64 GOAMD64=v1 goBuild temp/archtest .
GOARCH=arm GOARM=5 goBuild temp/archtest .
gzip -n -9 temp/*
mv temp/*.gz $MARCHAPP_DIR/

cd $HOME
mkdir -p $BINARY_DIR
echo 'Fetching sources files...'
git clone --single-branch --depth 1 -b $VERSION https://github.com/XTLS/Xray-core.git
cd Xray-core

BUILD_VERSION_TAG="$VERSION ($(git rev-parse HEAD | cut -b -8)) @ $(date -u +'%Y-%m-%d %H:%M:%S UTC')"
echo "Current Xray build version: $BUILD_VERSION_TAG"

echo 'Prepare all dependencies...'
go get -t -d ./...

echo 'Start building...'
for t in $TARGETS; do
  if [ "${t%/*}" = "windows" ]; then
    nameext='.exe'
  else
    nameext=''
  fi
  if [ "${t#*/}" = "amd64" ]; then
    for ma in $AMD64_MARCHES; do
      GOOS=${t%/*} GOARCH=amd64 GOAMD64=$ma goBuild $BINARY_DIR/xray ./main
    done
  elif [ "${t#*/}" = "arm" ]; then
    for ma in $ARM_MARCHES; do
      GOOS=${t%/*} GOARCH=arm GOARM=$ma goBuild $BINARY_DIR/xray ./main
    done
  else
    GOOS=${t%/*} GOARCH=${t#*/} goBuild $BINARY_DIR/xray ./main
  fi
done

echo "Compressing binary..."
gzip -n -9 $BINARY_DIR/*

echo "Downloading geosite and geoip data..."
mkdir $HOME/assets
cd $HOME/assets
wget -qOgeosite.dat https://github.com/v2fly/domain-list-community/releases/latest/download/dlc.dat
wget -qOgeoip.dat https://github.com/v2fly/geoip/releases/latest/download/geoip.dat
wget -qOcn-private.dat https://github.com/v2fly/geoip/releases/latest/download/geoip-only-cn-private.dat
wget -qOcn.dat https://github.com/v2fly/geoip/releases/latest/download/cn.dat
wget -qOprivate.dat https://github.com/v2fly/geoip/releases/latest/download/private.dat
tar --numeric-owner -c -z -f $HOME/geodata.tar.gz *.dat

#echo 'building geoip and domain-list-community...'
#go get -u -insecure github.com/v2ray/geoip
#go get -u -insecure github.com/v2ray/domain-list-community
#echo 'Fetching GeoLite2 data...'
#wget -qO- 'https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country-CSV&license_key=JvbzLLx7qBZT&suffix=zip' -o GeoLite2-Country-CSV.zip
#unzip -q GeoLite2-Country-CSV.zip
#rm -f GeoLite2-Country-CSV.zip
#mv GeoLite2* GeoLite2-Country-CSV
#cd $HOME/assets
#echo 'Generating geoip and geosite file...'
#geoip --country=../GeoLite2-Country-CSV/GeoLite2-Country-Locations-en.csv --ipv4=../GeoLite2-Country-CSV/GeoLite2-Country-Blocks-IPv4.csv --ipv6=../GeoLite2-Country-CSV/GeoLite2-Country-Blocks-IPv6.csv
#domain-list-community
#mv dlc.dat geosite.dat

echo 'Modify install script...'
sed -i 's|DOWNLOAD_BASE_URL=|\0"'$CI_JOB_URL/artifacts/raw'"|' $HOME/scripts/install.sh
sed -i "s|{BUILD_INFO}|$BUILD_VERSION_TAG|" $HOME/index.html
sed -i "s|{SCRIPT_URL_ROOT}|$CI_JOB_URL/artifacts/raw/scripts|" $HOME/index.html

cd $BINARY_DIR
if [ "$(ls -A .)" ]; then
  echo 'Build finished, waiting for artifacts storing process...'
  exit 0
fi

echo 'No build found at release directory!'
exit 1
