package main

import (
	"fmt"
	"runtime"

	"golang.org/x/sys/cpu"
)

func main() {
	var cpuVersion uint8
	switch runtime.GOARCH {
	case "amd64":
		cpuVersion = 1
		if cpu.X86.HasCX16 && cpu.X86.HasSSE3 && cpu.X86.HasSSE41 && cpu.X86.HasSSE42 && cpu.X86.HasPOPCNT && cpu.X86.HasSSSE3 {
			cpuVersion++
		}
		if cpu.X86.HasAVX && cpu.X86.HasAVX2 && cpu.X86.HasBMI1 && cpu.X86.HasBMI2 && cpu.X86.HasOSXSAVE {
			cpuVersion++
		}
		if cpu.X86.HasAVX512 && cpu.X86.HasAVX512BW && cpu.X86.HasAVX512CD && cpu.X86.HasAVX512DQ && cpu.X86.HasAVX512VL {
			cpuVersion++
		}
	case "arm":
		cpuVersion = 5
		if cpu.ARM.HasVFP {
			cpuVersion++
		}
		if cpu.ARM.HasVFPv3 {
			cpuVersion++
		}
	default:
		cpuVersion = 0
	}
	fmt.Print(cpuVersion)
}
