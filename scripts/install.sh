#!/bin/bash

set -e

DOWNLOAD_BASE_URL=

EXEC_PATH=binary/xray-linux
ENV_PREFIX=XRAY

if type curl > /dev/null 2>&1; then
  DOWNLOADER='curl -sL'
elif type wget > /dev/null 2>&1; then
  DOWNLOADER='wget -qO-'
else
  echo -e "\e[31mCan not download file: curl/wget not found\e[0m" >&2
  exit 1
fi

install_service=yes
install_geodat=yes
service_file=/etc/systemd/system/raycore.service

while [ $# -gt 0 ]; do
  if [ "$1" = "-S" ]; then
    install_service=no
  elif [ "$1" = "-G" ]; then
    install_geodat=no
  fi
  shift 1
done

fetchFile() {
  if [ "x$2" = "x" ]; then
    $DOWNLOADER $1
  else
    $DOWNLOADER $1 | gzip -c -d - >$2
  fi
}

test -e /etc/raycore/config.d || mkdir -p /etc/raycore/config.d
arch=`uname -m`
case "$arch" in
    i?86) arch=386 ;;
    x86_64) arch=x86_64-v1 ;;
    armv6l) arch=armv6 ;;
    armv7l) arch=armv7 ;;
    aarch64) arch=arm64 ;;
    armv8l) arch=arm64 ;;
esac
work_dir=/tmp/$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c16)
mkdir -p $work_dir
cd $work_dir

if [ "$arch" = "x86_64-v1" ]; then
  fetchFile $DOWNLOAD_BASE_URL/marchbin/archtest-linux-x86_64-v1.gz ./archtest
  chmod +x ./archtest
  amd64ver=$(./archtest)
  if [ "$amd64ver" = "2" ]; then
    arch=x86_64-v2
  elif [ "$amd64ver" = "3" ]; then
    arch=x86_64-v3
  elif [ "$amd64ver" = "4" ]; then
    arch=x86_64-v4
  fi
fi
echo -e "\e[36mCurrent platform architecture is $arch\e[0m"

echo -e '\e[36mDownloading binary...\e[0m'
fetchFile "$DOWNLOAD_BASE_URL/$EXEC_PATH-$arch.gz" ./raycore
install ./raycore /usr/local/bin

if [ "$install_service" = "yes" ]; then
  cat << EOF > $service_file
[Unit]
Description=A unified platform for anti-censorship
After=network-online.target
Wants=network-online.target

[Service]
User=nobody
EnvironmentFile=-/etc/raycore/env
ExecStart=/usr/local/bin/raycore run
Restart=on-failure
WorkingDirectory=/etc/raycore
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE CAP_NET_RAW CAP_DAC_READ_SEARCH
AmbientCapabilities=CAP_NET_ADMIN CAP_NET_BIND_SERVICE CAP_NET_RAW CAP_DAC_READ_SEARCH
LimitNOFILE=infinity

[Install]
WantedBy=multi-user.target
EOF
  chmod 0644 $service_file
  systemctl daemon-reload || true
fi

if [ ! -e /etc/raycore/config.json ]; then
  echo -e "\e[32mYou may need creating your configs\e[0m"
fi

if [ ! -e /etc/raycore/env ]; then
  cat << EOF > /etc/raycore/env
# Environment variables for $ENV_PREFIX
${ENV_PREFIX}_BUF_READV=enable
# ${ENV_PREFIX}_CONF_GEOLOADER=standard
${ENV_PREFIX}_LOCATION_ASSET=/etc/raycore
${ENV_PREFIX}_LOCATION_CONFIG=/etc/raycore
${ENV_PREFIX}_LOCATION_CONFDIR=/etc/raycore/config.d
# ${ENV_PREFIX}_RAY_BUFFER_SIZE=2
EOF
fi

if [ "$install_geodat" = "yes" ]; then
  echo -e "\e[36mDownloading and installing GEO data\e[0m"
  fetchFile $DOWNLOAD_BASE_URL/geodata.tar.gz | tar xz
  cp -f geoip.dat /etc/raycore
  cp -f geosite.dat /etc/raycore
  cp -f cn.dat /etc/raycore
  cp -f private.dat /etc/raycore
fi

if [ ! -e /etc/raycore/cert.pem ]; then
  echo -e "\e[36mGenerating a self-signed certificate\e[0m"
  openssl ecparam -genkey -name prime256v1 -out /etc/raycore/key.pem
  openssl req -key /etc/raycore/key.pem -x509 -days 1826 -subj "/C=US/ST=Utah/L=Orem/O=V2/OU=Org/CN=*" -out /etc/raycore/cert.pem
  chmod 0644 /etc/raycore/key.pem
fi

rm -rf $work_dir

echo -e "\e[32mUse 'systemctl start raycore' to start service.\e[0m"
