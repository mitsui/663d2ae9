#!/bin/sh
systemctl disable raycore || true
systemctl stop raycore || true
rm -f /usr/local/bin/raycore
rm -f /etc/systemd/system/raycore.service
rm -rf /etc/raycore
